<?php

namespace Drupal\contextual_comparison\Plugin\views\argument;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;

/**
 * Argument handler extends the default numeric handler with the addition of
 * comparison operators.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("numeric_comparison")
 */
class NumericComparisonArgument extends NumericArgument {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['comparison'] = array(
      'contains' => array(
        'comparison_type' => array('default' => '='),
        'allow_multiple' => array('default' => FALSE),
      ),
    );
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['comparison'] = array(
      '#type' => 'details',
      '#title' => $this->t('Comparison'),
    );
    $form['comparison']['comparison_type'] = array(
      '#type' => 'select',
      '#title' => t('Filter Type'),
      '#description' => t('Select how content should be filtered against this argument. Only works for single values. Multiple values will fall back to usual behaviour.'),
      '#options' => array(
        'equal' => '=',
        'not_equal' => '<>',
        'gt_or_equal' => '>=',
        'lt_or_equal' => '<=',
        'gt' => '>',
        'lt' => '<',
      ),
      '#default_value' => !empty($this->options['comparison']['comparison_type']) ? $this->options['comparison']['comparison_type'] : NULL,
    );

    // Move the 'Allow multiple values' options into our comparison section.
    unset($form['break_phrase']['#group']);
    $form['comparison']['allow_multiple'] = $form['break_phrase'];
    unset($form['break_phrase']);

    // Make it visible only if a filter type of equal or not equal is selected.
    $form['comparison']['allow_multiple']['#states'] = array(
      'visible' => array(
        ':input[name="options[comparison][comparison_type]"]' => array(
          array('value' => 'equal'),
          array('value' => 'not_equal'),
        ),
      )
    );

    // Remove old 'not' options as this is covered by the not equal comparison.
    unset($form['not']);
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    $this->ensureMyTable();

    if (!empty($this->options['comparison']['allow_multiple'])) {
      $break = static::breakString($this->argument, FALSE);
      $this->value = $break->value;
      $this->operator = $break->operator;
    }
    else {
      $this->value = array($this->argument);
    }

    $placeholder = $this->placeholder();
    $null_check = $this->options['comparison']['comparison_type'] == '=' ? '' : "OR $this->tableAlias.$this->realField IS NULL";

    if (count($this->value) > 1) {
      $operator = $this->options['comparison']['comparison_type'] == '=' ? 'IN' : 'NOT IN';
      $placeholder .= '[]';
      $this->query->addWhereExpression(0, "$this->tableAlias.$this->realField $operator($placeholder) $null_check", array($placeholder => $this->value));
    }
    else {
      // Single value, so check for selected comparison type
      switch ($this->options['comparison']['comparison_type']) {
        case 'equal':
        default:
          $operator = '=';
          break;
        case 'not_equal':
          $operator = '<>';
          break;
        case 'gt_or_equal':
          $operator = '>=';
          break;
        case 'lt_or_equal':
          $operator = '<=';
          break;
        case 'gt':
          $operator = '>';
          break;
        case 'lt':
          $operator = '<';
          break;
      }

      // Add the query condition
      $this->query->addWhereExpression(0, "$this->tableAlias.$this->realField $operator $this->argument");
    }
  }

}